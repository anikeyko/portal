"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from main import settings
from .yasg import urlpatterns as doc_urls

urlpatterns = [
    path('admin/', admin.site.urls),
]

urlpatterns += doc_urls

api_urlpatterns = []
for app in settings.API_APPS:
    namespace = app.replace('.', '_')
    url_path = f'{app}.api.urls'
    api_urlpatterns.append(
        path(f'api/{app}/', include(url_path, namespace=namespace))
    )
urlpatterns += tuple(api_urlpatterns)