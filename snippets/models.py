from django.db import models


class CreatedModel(models.Model):
    created_at = models.DateTimeField("Создано", auto_now_add=True)

    class Meta:
        abstract = True


class UpdatedModel(CreatedModel):
    updated_at = models.DateTimeField("Обновлено", auto_now=True)

    class Meta:
        abstract = True
