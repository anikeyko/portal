from django.urls import path, include
from rest_framework.routers import DefaultRouter

from bookmarks.api import resources

app_name = 'bookmarks'

router = DefaultRouter()

router.register('', resources.BookmarkViewSet, basename='bookmarks')
router.register('folders', resources.FolderViewSet, basename='folders')

urlpatterns = (
    path(
        '',
        include(router.urls)
    ),
)
