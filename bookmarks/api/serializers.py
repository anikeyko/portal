from rest_framework import serializers

from bookmarks.models import Bookmark, Folder


class BookmarkSerializer(serializers.ModelSerializer):
    """Сериализатор для закладки"""

    class Meta:
        model = Bookmark
        fields = "__all__"


class BookmarkCreateSerializer(serializers.ModelSerializer):
    """Созадние закладки"""

    class Meta:
        model = Bookmark
        fields = "__all__"


class FolderSerializer(serializers.ModelSerializer):
    """Сериализатор для директории"""

    class Meta:
        model = Folder
        fields = "__all__"
