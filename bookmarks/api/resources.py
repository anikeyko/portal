from rest_framework.viewsets import ModelViewSet

from bookmarks.api.serializers import BookmarkSerializer, FolderSerializer
from bookmarks.models import Bookmark, Folder


class BookmarkViewSet(ModelViewSet):
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer


class FolderViewSet(ModelViewSet):
    queryset = Folder.objects.all()
    serializer_class = FolderSerializer
