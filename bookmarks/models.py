from django.db import models


class Folder(models.Model):
    """Папка"""
    title = models.CharField("Название", max_length=255)
    slug = models.CharField("Алиас", max_length=255)

    class Meta:
        verbose_name = "Директория"
        verbose_name_plural = "Директории"
        app_label = "bookmarks"

    def __str__(self):
        return self.title


class Bookmark(models.Model):
    """Закладка"""
    title = models.CharField("Название", max_length=255, blank=False)
    url = models.CharField("URL", max_length=500, blank=False)
    is_favorite = models.BooleanField("Избранное", default=False)
    folder = models.ForeignKey(Folder, verbose_name="Директория", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Закладка"
        verbose_name_plural = "Закладки"
        app_label = "bookmarks"

    def __str__(self):
        return self.title
