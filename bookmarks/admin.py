from django.contrib import admin

# Register your models here.
from bookmarks.models import Bookmark, Folder


@admin.register(Bookmark)
class BookmarkAdmin(admin.ModelAdmin):
    pass


@admin.register(Folder)
class FolderAdmin(admin.ModelAdmin):
    pass
