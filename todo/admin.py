from django.contrib import admin

# Register your models here.
from todo.models import Task, TaskList


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("title", "parent", "created_at")


@admin.register(TaskList)
class TaskListAdmin(admin.ModelAdmin):
    list_display = ("title", "created_at")
