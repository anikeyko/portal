from django.db import models

# Create your models here.
from snippets.models import CreatedModel


class TaskList(CreatedModel, models.Model):
    """Список задач"""
    title = models.CharField("Заголовок", max_length=255)
    status = models.IntegerField("Статус")

    class Meta:
        verbose_name = "Список задач"
        verbose_name_plural = "Списки задач"

    def __str__(self):
        return self.title


class Task(CreatedModel):
    """Задача"""
    title = models.CharField("Заголовок", max_length=255)
    content = models.TextField("Содержимое", blank=True)
    status = models.IntegerField("Статус")
    parent = models.ForeignKey(TaskList, verbose_name="Список", on_delete=models.CASCADE, related_name='tasks')

    class Meta:
        verbose_name = "Задача"
        verbose_name_plural = "Задачи"

    def __str__(self):
        return self.title