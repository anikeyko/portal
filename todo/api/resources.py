from rest_framework.viewsets import ModelViewSet

from todo.api.serializers import TaskSerializer, TaskListSerializer
from todo.models import Task, TaskList


class TaskViewSet(ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class TaskListViewSet(ModelViewSet):
    queryset = TaskList.objects.all()
    serializer_class = TaskListSerializer
