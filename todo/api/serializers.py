from rest_framework import serializers

from todo.models import Task, TaskList


class TaskSerializer(serializers.ModelSerializer):
    """Сериализатор для задачи"""

    class Meta:
        model = Task
        fields = "__all__"


class TaskListSerializer(serializers.ModelSerializer):
    """Сериализатор для списка задач"""

    class Meta:
        model = TaskList
        fields = "__all__"
