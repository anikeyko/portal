from django.urls import path, include
from rest_framework.routers import DefaultRouter

from todo.api import resources

app_name = 'todo'

router = DefaultRouter()

router.register('tasks', resources.TaskViewSet, basename='tasks')
router.register('lists', resources.TaskListViewSet, basename='lists')

urlpatterns = (
    path(
        '',
        include(router.urls)
    ),
)
